Sorry for the last section. I was very tired, and I think that you might have missed some of the key points behind this very important topic: linearization. I decided to write down what I believe to be very important. I still will revise it tomorrow, or the next week though. 

In typical surveying works we often run it situations where we need to use the least squares to adjust some nonlinear functions. You often see that in triangulation and traverse networks. In geodetic netowrks you also need to do least squares in nonlinear set of equations. In this section we are trying to get more intuitions about the use of least squares in nonlinear equations. 

```math
AX = L,
```
which is our old least squares equation. To see exactly when the _nonlinearty_ issue arises, let us see this example.

```math
\begin{aligned}
x + y - 2y^2 &= -4
x^2 + y^2 &= 8
3x^2 - y^2 &= 7.7
\end{aligned}
```

and this example.


```math
\begin{aligned}
A x_a^2 + B x_a + C &= y_a + \rho_a
A x_b^2 + B x_b + C &= y_b + \rho_b
A x_c^2 + B x_c + C &= y_c + \rho_c
A x_d^2 + B x_d + C &= y_d + \rho_d
A x_e^2 + B x_e + C &= y_e + \rho_e
\end{aligned}
```
